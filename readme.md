# Laravel Young America

Integrate with Young America, Create new Users and send entry details.

[TOC]

---
## Installation

### 1) Include Package With Composer

| Laravel Version | Package Version |
|-----------------|-----------------|
| 5.2             | 1.0.*           |

__Example__
```json
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/14fourdev/laravel-young-america.git"
    }
  ],
  "require": {
    "14four/laravel-young-america": "~1.0"
  }
}
```

__Run Compose__
```
php composer update
```

### 2) Add Service Provider and Facade

__{laravelroot}/config/app.php__

This will allow the config for Young America to be published and provide a `YoungAmerica` facade so you can access the class without requiring it.

```php
<?php

return [
    ...
    'providers' => [
        ...
        YoungAmerica\YoungAmericaServiceProvider::class,
        ...
    ],
    ...
    'aliases' => [
        ...
        'YoungAmerica' => YoungAmerica\YoungAmerica::class,
        ...
    ],
];
```

### 3) Publish Config File

```
php artisan vendor:publish --provider="YoungAmerica\YoungAmericaServiceProvider"
```

### 4) Update Config With your Details

__{laravelroot}/config/youngamerica.php__

This will provide the credentials to your Program to Young America. This is __Required__ for the package to work.

```php
<?php

return [
    ...
    'client' => env('YOUNGAMERICA_CLIENT', ${CLIENT ID GOES HERE}),
    ...
    'vendor' => env('YOUNGAMERICA_VENDOR', ${VENDOR ID GOES HERE}),
    ...
];
```

***note*** If your in an development environment set the `YOUNGAMERICA_ENDPOINT` in your `.env` to the development endpoint.
```
YOUNGAMERICA_ENDPOINT=http://e-webservices.yastaging.com/SubmissionServices.asmx?wsdl
```

## Usage

### Create a New Consumer

```php
<?php
...
    public function yourController() {
        ...

        $newConsumer = [
            'FirstName' => 'Albert',
            'LastName' => 'Dev',
            'Address1' => '244 W Main',
            'City' => 'Seattle',
            'State' => 'WA',
            'PostalCode' => '99999',
            'CountryCode' => 'US',
            'Email' => 'albert@example.com',
            'Phone' => '555-555-5555',
            'DateOfBirth' => '2016-08-11',
        ];

        $youngAmerica = new YoungAmerica;

        $consumer = $youngAmerica->setOfferId( $offerID )->setSecurityToken( $securityToken )->createConsumer( $newConsumer );

        ...
    }
...
```
***note*** this is an example formatting of entries should be specific to your Program


### Create a New Entry

```php
<?php
...
    public function yourController() {
        ...

        $newEntry = [
            'DailyEntryDetail' => [
                'ExternalID' => $consumer->email,
                'EntryDate' => date('m/d/Y H:i:s A');
            ]
        ];

        $youngAmerica = new YoungAmerica;

        $consumer = $youngAmerica->setOfferId( $offerID )->setSecurityToken( $securityToken )->createEntry( $newEntry );

        ...
    }
...
```
***note*** this is an example formatting of entries should be specific to your Program


# Known Limitations
* Only one Consumer or Entry can be created at a time.

# TODO
* Create ability to add multiple entries at one time.
* Write Test
