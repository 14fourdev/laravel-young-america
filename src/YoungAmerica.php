<?php namespace YoungAmerica;

use SoapClient;
use SoapVar;
use App;
use Log;
use YoungAmerica\Helpers\Xml;

/**
 * Connect to Young America and Post new Client
 *
 */
class YoungAmerica {

    /**
     * Offer Id
     */
    private $offerId;


    /**
     * Security Token
     */
    private $securityToken;


    /**
     * Count of records to be entered
     */
    private $count = 1;



    /**
     * Construct the Young America Class and Verify that config is set.
     */
    public function __construct() {

        if( !config('youngamerica.client') ) {
            App::abort(500, 'Client ID not set in /config/youngamerica.php');
        }

        if( !config('youngamerica.vendor') ) {
            App::abort(500, 'Vendor not set in /config/youngamerica.php');
        }

    }


    /**
     * Set The Offer ID, This is required to post to Young America
     * @param string|int $offerId ID of the offer beign entered into
     * @return object       This Object for daisy chaining commands
     */
    public function setOfferId( $offerId ) {

        $this->offerId = $offerId;

        return $this;
    }


    /**
     * Security Token for Young America, this is required to post
     * @param string|int $securityToken Young America SecurityToken beign entered into
     * @return object       This Object for daisy chaining commands
     */
    public function setSecurityToken( $securityToken ) {

        $this->securityToken = $securityToken;

        return $this;
    }


    /**
     * Verify Program components have been configured and set
     * @return bool True  if program was configured.
     */
    public function verifyProgram( ) {

        if ( !$this->offerId ) {
            App::abort(500, 'Offer ID needs to be configured before you can send data to Young America.');
        } else if ( !is_numeric($this->offerId) ) {
            App::abort(500, 'Offer ID needs to be numeric.');
        }

        if ( !$this->securityToken ) {
            App::abort(500, 'Security Token needs to be configured before you can send data to Young America.');
        }

        return true;

    }


    /**
     * Return the header for the request
     * @return Array Structure and Data for the header to be generated
     */
    public function getHeader( $action = 'ConsumerInsertEntry' ) {

        return [
            'RequestHeader' => [
                'ClientID' => config('youngamerica.client'),
                'OfferID' => $this->offerId,
                'SecurityToken' => $this->securityToken,
                'Action' => $action,
                'VendorCode' => config('youngamerica.vendor'),
                'RecordCount' => $this->count,
                'ReturnIdsFlag' => 'True',
            ]
        ];

    }


    /**
     * Create A New Customer In Youung America's System
     * @param Array $user Key Value pair for information that should be entered into Young America
     * @return Array Success or Error with the results from the post to Young America
     */
    public function createConsumer( $user = [] ) {

        $this->verifyProgram();

        $xml = new Xml('YASubmissionRequest');

        $xml->createChild( $this->getHeader( 'ConsumerInsert' ) );

        $xml->createChild([ 'ConsumerData' => $user ]);

        $result = $this->sendRequest( $xml );

        return $result;

    }


    /**
     * Create A new Entry for a user in Young America
     * @param Array $user Key Value pair for information that should be entered into Young America
     * @return Array Success or Error with the results from the post to Young America
     */
    public function createEntry( $entry = [] ) {

        $this->verifyProgram();

        $xml = new Xml('ConsumerAddEntryRequest');

        $xml->createChild( $this->getHeader() );

        $xml->createChild([ 'DailyEntries' => $entry ]);

        $result = $this->sendRequest( $xml );

        return $result;
    }


    /**
     * Send the XML request to Young america and check the response.
     * @param Object $xml Xml object that contains post information;
     * @return Array Success or Error with the results from the post to Young America
     */
    private function sendRequest( Xml $xml ) {

        $result = false;

        try{

            $client = new SoapClient(config('youngamerica.endpoint'), ['trace' => true]);

            $client->debug_flag = true;

            $input = new SoapVar('<ProcessRequest xmlns="http://YA.SubmissionApi.Services/"><requestXml>'.htmlentities( $xml->toString() ).'</requestXml></ProcessRequest>', XSD_ANYXML);

            $body = $client->ProcessRequest($input);

        } catch (\SoapFault $exception) {

            Log::error('Young America Send Failed', ['exception' => $exception, 'xml' => $xml]);

        }

        $result = [];

        if($body) {

            $result = json_decode(json_encode( (array)simplexml_load_string($body->ProcessRequestResult) ), 1);

        }

        $xml = false;

        if ( $result && $result['Status'] == 'Success' ) {

            return ['success' => true, 'response' => $result];

        } else if ( $result ) {

            Log::error('Young America Error: ' . print_r($result, true));

            return ['success' => false, 'response' => $result];

        }

        Log::error('No Response from Young America');

        App::abort(500, 'No Response from Young America');

    }


}
