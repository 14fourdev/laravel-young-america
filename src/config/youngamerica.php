<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client configuration
    |--------------------------------------------------------------------------
    |
    | Set client ID for the campaigns Young America Account. Setup override
    | in your .env file using YOUNGAMERICA_CLIENT
    |
    */
    'client' => env('YOUNGAMERICA_CLIENT', null),

    /*
    |--------------------------------------------------------------------------
    | Vendor ID
    |--------------------------------------------------------------------------
    |
    | Set the Vendor ID for your Young America Project. Setup override in your
    | .env file using YOUNGAMERICA_VENDOR
    |
    */
    'vendor' => env('YOUNGAMERICA_VENDOR', null),

    /*
    |--------------------------------------------------------------------------
    | ENDPOINT
    |--------------------------------------------------------------------------
    |
    | Set the endpoint that the Young America API is going to hit. Setup
    | Override in your .env file. Default is production, for development
    | set this endpoint to
    |
    */
    'endpoint' => env('YOUNGAMERICA_ENDPOINT', 'https://webservices.young-america.com/SubmissionServices.asmx?wsdl'),

];
