<?php namespace YoungAmerica\Helpers;

use SimpleXMLElement;

/**
 * XML
 */
class Xml {

    /**
     * Single XML SimpleXMLElement
     */
    private $xml;


    /**
     * Construct the SimpleXMLElement object, pass in a parent wrapper
     * @param string $parent Name of the parent node of the XML object.
     */
    public function __construct( $parent ) {

        $this->xml = new SimpleXMLElement('<'.$parent.'></'.$parent.'>');

    }


    /**
     * Create a Child node to the parent Node
     * @param Array $data Array of Key Vallue pairs for generating children
     * @param Object $node Node of the children for multi level children
     * @return void
     */
    public function createChild( $data = [], $node = null ) {

        if ( is_null( $node ) ) {
            $node = $this->xml;
        }

        foreach ( $data as $key => $value ) {
            if ( is_array($value) ) {
                $subNode = $node->addChild( $key );
                $this->createChild($value, $subNode);
            } else {
                $node->addChild((string)$key, htmlspecialchars((string)$value));
            }
        }

    }


    /**
     * Retrun A string of the XML object
     * @return string String of XML formatted.
     */
    public function toString() {

        return $this->xml->asXML();
    }


}
