<?php
namespace YoungAmerica;

use YoungAmerica;
use Illuminate\Support\ServiceProvider;

class YoungAmericaServiceProvider extends ServiceProvider {


    /**
     * Defer the loading of this service provider until it's called
     */
    // protected $defer = true;

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() {

        // Provide Young America
        $this->app->bind('YoungAmerica', function ($app) {
            return new YoungAmerica();
        });


    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() {

        $this->publishes([
            __DIR__ . '/config/youngamerica.php' => config_path('youngamerica.php'),
        ], 'config');

    }

}
